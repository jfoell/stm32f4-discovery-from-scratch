#pragma once

enum {
	LED_1_PORT = 'G',
	LED_1_PIN  = 13,

	LED_2_PORT = 'G',
	LED_2_PIN  = 14,
};

#define BOARD_LED_TBL                                                          \
{                                                                              \
	{ LED_1_PORT, LED_1_PIN, },                                                \
	{ LED_2_PORT, LED_2_PIN, },                                                \
}


