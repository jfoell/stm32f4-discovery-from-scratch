#include <stdint.h>

int
arm_reg_readu32(uint32_t addr, void * result)
{
	volatile uint32_t * reg = (uint32_t *)addr;

	if (result)
	{
		*((uint32_t *)result) = (*reg);
	}

	return 0;
}

int
arm_reg_readu16(uint32_t addr, void * result)
{
	volatile uint16_t * reg = (uint16_t *)addr;

	if (result)
	{
		*((uint16_t *)result) = (*reg);
	}

	return 0;
}

int
arm_reg_readu8(uint32_t addr, void * result)
{
	volatile uint8_t * reg = (uint8_t *)addr;

	if (result)
	{
		*((uint8_t *)result) = (*reg);
	}

	return 0;
}

int
arm_reg_writeu32(uint32_t addr, uint32_t val, void * result)
{
	volatile uint32_t * reg = (uint32_t *)addr;

	(*reg) = val;

	if (result)
	{
		*((uint32_t *)result) = (*reg);
	}

	return 0;
}

int
arm_reg_writeu16(uint32_t addr, uint16_t val, void * result)
{
	volatile uint16_t * reg = (uint16_t *)addr;

	(*reg) = val;

	if (result)
	{
		*((uint16_t *)result) = (*reg);
	}

	return 0;
}

int
arm_reg_writeu8(uint32_t addr, uint8_t val, void * result)
{
	volatile uint8_t * reg = (uint8_t *)addr;

	(*reg) = val;

	if (result)
	{
		*((uint8_t *)result) = (*reg);
	}

	return 0;
}

