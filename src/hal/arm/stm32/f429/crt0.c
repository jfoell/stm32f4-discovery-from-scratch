
#include <stdint.h>

#include "toolchain.h"
#include "arm_util.h"

/* These MUST be set properly in linker script. */
extern const void * _etext;
extern void * _data;
extern void * _edata;
extern void * _bss;
extern void * _ebss;
extern void * _stack;
extern void * _estack;

void
TOOLCHAIN_NAKED
TOOLCHAIN_NORETURN
crt0_start(void)
{
	const void * src;
	void * dst;
	uint32_t size;

	/* copy 'data' from flash to RAM */
	src = &_etext;
	dst = &_data;
	size = (uint32_t)&_edata - (uint32_t)&_data;
	arm_util_memcpy(dst, src, size, 0);

	/* set the 'bss' to zeroes */
	dst = &_bss;
	size = (uint32_t)&_ebss - (uint32_t)&_bss;
	arm_util_memset(dst, 0, size, 0);

	/* set the 'stack' to a distinct pattern */
	/* @WARNING
	 * Need to find a way to do this without corrupting the stack frame.
	 *
	dst = &_stack;
	size = (uint32_t)&_estack - (uint32_t)&_stack;
	arm_util_memset(dst, 0xa5, size, 0);
	 *
	 */

	extern void main(void);
	main();
	while (1);
}

