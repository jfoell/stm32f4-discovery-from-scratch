#include <stdint.h>

#include "arm_bba.h"

/*
 * Glossary
 *
 * AHB = Advanced High-performance Bus
 * APB = Advanced Peripheral Bus
 */

enum {
	RCC_BASE                   = 0x40023800,
	RCC_OFFS_AHB1ENR           = 0x030,
};

enum {
	AHB_ID_UNKNOWN = 0,
	AHB_ID_1 = 1,
	AHB_ID_2 = 2,
	AHB_ID_3 = 3,
	AHB_ID_MAX,
};

enum {
	APB_ID_UNKNOWN = 0,
	APB_ID_1 = 1,
	APB_ID_2 = 2,
	APB_ID_3 = 3,
	APB_ID_MAX,
};

int
bus_clock_ahb_enable(int id, int bit_position, void * result)
{
	if ((id < AHB_ID_1) || (id >= AHB_ID_MAX))
	{
		return -1;
	}
	else if ((bit_position < 0) || (bit_position >= 32))
	{
		return -1;
	}
	else
	{
		uint32_t const addr = RCC_BASE + RCC_OFFS_AHB1ENR + (4 * (id - 1));
		return arm_bba_writeu32(addr, bit_position, 1, 0);
	}
}

int
bus_clock_apb_enable(int id, void * result)
{
	if (-1)
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

