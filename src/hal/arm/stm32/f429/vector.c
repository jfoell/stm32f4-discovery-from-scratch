#include <stdint.h>

#include "chip.h"
#include "toolchain.h"

void
TOOLCHAIN_ISR
vector_isr_default(void)
{
	while (1);
}

void
TOOLCHAIN_ISR
TOOLCHAIN_NORETURN
TOOLCHAIN_NAKED
vector_isr_reset(void)
{
	extern void premain(void);

	premain();
	while (1);
}

TOOLCHAIN_SECTION(".vector")
static void (*vector_tbl[N_ISR_VECTOR])(void) =
{
	(void (*)(void))(CHIP_SRAM_BASE + CHIP_SRAM_SZ),
	vector_isr_reset,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
	vector_isr_default,
};

