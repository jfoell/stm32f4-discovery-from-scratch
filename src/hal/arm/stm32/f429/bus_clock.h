#pragma once

int bus_clock_ahb_enable(int id, int bit_position, void * result);

int bus_clock_apb_enable(int id, void * result);

