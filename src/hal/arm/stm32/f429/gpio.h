
#pragma once


int gpio_port_enable(int portchar, void * result);

int gpio_mode_setout(int portchar, int pin, void * result);

int gpio_write(int portchar, int pin, int value, void * result);

