/*
 * definitions for STM32F4129(ZI)
 *
 * This file may work for other STM32F4 variants.  Assume that it is ONLY for
 * STM32F429ZI.
 */

enum {
	CHIP_SRAM_BASE             = 0x20000000,
	CHIP_SRAM_SZ               = (192 * 1024),
	CHIP_FLASH_SZ              = ((1024 + 512) * 1024),

	N_ISR_VECTOR               = (16 + 91),
};

