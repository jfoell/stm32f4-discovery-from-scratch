#include <stdint.h>

#include "arm_bba.h"
#include "bus_clock.h"

enum {
	GPIO_PORTA =  0,
	GPIO_PORTB =  1,
	GPIO_PORTC =  2,
	GPIO_PORTD =  3,
	GPIO_PORTE =  4,
	GPIO_PORTF =  5,
	GPIO_PORTG =  6,
	GPIO_PORTH =  7,
	GPIO_PORTI =  8,
	GPIO_PORTJ =  9,
	GPIO_PORTK = 10,
};

enum {
	GPIO_BASE                  = 0x40020000,
	GPIO_OFFS_MODER            = 0x000,
	GPIO_OFFS_ODR              = 0x014,
};

static int
gpio_port_id(int portchar, void * result)
{
	if ((portchar < 'A') || (portchar > 'K'))
	{
		return -1;
	}
	else
	{
		if (result)
		{
			*((int *)result) = (portchar - (int)'A');
		}
		return 0;
	}
}

static int
gpio_port_ahb_bit(int portchar, void * result)
{
	/* same as the ID... nice job ST */
	return gpio_port_id(portchar, result);
}

static int
gpio_port_base(int portchar, void * result)
{
	int port;

	if (gpio_port_id(portchar, &port))
	{
		return -1;
	}
	else
	{
		uint32_t port_base;

		port_base = GPIO_BASE + (port * 0x400);
		if (result)
		{
			*((uint32_t *)result) = port_base;
		}

		return 0;
	}
}

int
gpio_port_enable(int portchar, void * result)
{
	int ahb_bit;

	if (gpio_port_ahb_bit(portchar, &ahb_bit))
	{
		return -1;
	}
	else
	{
		return bus_clock_ahb_enable(1, ahb_bit, 0);
	}
}

int
gpio_mode_setout(int portchar, int pin, void * result)
{
	uint32_t port_base;

	if (gpio_port_base(portchar, &port_base))
	{
		return -1;
	}
	else if ((pin < 0) || (pin > 15))
	{
		return -2;
	}
	else
	{
		uint32_t addr = port_base + GPIO_OFFS_MODER;
		int mode_position = pin * 2;

		if (arm_bba_writeu32(addr, (mode_position + 0), 1, 0) ||
		    arm_bba_writeu32(addr, (mode_position + 1), 0, 0))
		{
			return -3;
		}
		else
		{
			return 0;
		}
	}
}

int
gpio_write(int portchar, int pin, int value, void * result)
{
	uint32_t port_base;

	if (gpio_port_base(portchar, &port_base))
	{
		return -1;
	}
	else if ((pin < 0) || (pin > 15))
	{
		return -2;
	}
	else
	{
		uint32_t addr = port_base + GPIO_OFFS_ODR;

		return arm_bba_writeu32(addr, pin, (value != 0), 0);
	}
}

