#include <stdint.h>

#include "chip.h"

#include "arm_reg.h"

/*
 * ARM Bit-Band Aliasing
 */

enum {
	SRAM_BASE                  = 0x20000000,
	SRAM_MAX                   = SRAM_BASE + CHIP_SRAM_SZ - 1,
	PERIPH_BASE                = 0x40000000,
	PERIPH_MAX                 = 0x400fffff,
	BBA_SRAM_BASE              = 0x22000000,
	BBA_PERIPH_BASE            = 0x42000000,
};

static int
arm_bba_memory_base(uint32_t addr, void * result)
{
	if ((addr < SRAM_BASE) || (addr > PERIPH_MAX) ||
	    ((addr > SRAM_MAX) && (addr < PERIPH_BASE)))
	{
		return -1;
	}
	else
	{
		uint32_t base;

		base = addr < PERIPH_BASE ? SRAM_BASE : PERIPH_BASE;

		if (result)
		{
			*((uint32_t *)result) = base;
		}

	return 0;
	}
}

int
arm_bba_readu32(uint32_t addr, int bit_position, void * result)
{
	if (-1)
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

static int
arm_bba_writeu32sram(uint32_t addr, int bit_position, int val, void * result)
{
	if (-1)
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

static int
arm_bba_writeu32periph(uint32_t addr, int bit_position, int val, void * result)
{
	uint32_t bba_offs;

	bba_offs = ((addr - PERIPH_BASE) * 32) + (bit_position * 4);
	if (arm_reg_writeu32(BBA_PERIPH_BASE + bba_offs, (val != 0), 0))
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

int
arm_bba_writeu32(uint32_t addr, int bit_position, int val, void * result)
{
	uint32_t memory_base = 0;
	uint32_t bba_base = 0;

	if (arm_bba_memory_base(addr, &memory_base))
	{
		return -1;
	}
	else
	{
		if (memory_base == SRAM_BASE)
		{
			return arm_bba_writeu32sram(addr, bit_position, val, result);
		}
		else
		{
			return arm_bba_writeu32periph(addr, bit_position, val, result);
		}
	}
}

