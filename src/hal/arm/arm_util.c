#include <stdint.h>

int
arm_util_memcpy(void * dst, const void * src, uint32_t size, void * result)
{
	uint8_t * d = (uint8_t *)dst;
	const uint8_t * s = (const uint8_t *)src;

	while (size)
	{
		if ((size < 4) || ((uint32_t)d & 0x3) || ((uint32_t)s & 0x3))
		{
			/* byte-by-byte copy */
			(*d) = (*s);
			d++;
			s++;
			size--;
		}
		else
		{
			/* word-by-word copy */
			uint32_t * d_word = (uint32_t *)d;
			uint32_t * s_word = (uint32_t *)s;

			(*d_word) = (*s_word);
			d += 4;
			s += 4;
			size -= 4;
		}
	}

	if (result)
	{
		*((void **)result) = dst;
	}

	return 0;
}

int
arm_util_memset(void *dst, int byteval, uint32_t size, void * result)
{
	uint8_t * d = (uint8_t *)dst;
	const uint8_t val = byteval & 0xff;
	const uint32_t val_word = (((uint32_t)val << 24) |
	                           ((uint32_t)val << 16) |
	                           ((uint32_t)val <<  8) |
	                           ((uint32_t)val <<  0));

	while (size)
	{
		if ((size < 4) || ((uint32_t)d & 0x3))
		{
			/* byte-by-byte set */
			(*d) = val;
			d++;
			size--;
		}
		else
		{
			/* word-by-word set */
			uint32_t * d_word = (uint32_t *)d;

			(*d_word) = val_word;
			d += 4;
			size -= 4;
		}
	}

	if (result)
	{
		*((void **)result) = dst;
	}

	return 0;
}

