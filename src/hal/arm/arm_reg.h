
#pragma once

#include <stdint.h>

int arm_reg_readu32(uint32_t addr, void * result);

int arm_reg_readu16(uint32_t addr, void * result);

int arm_reg_readu8(uint32_t addr, void * result);

int arm_reg_writeu32(uint32_t addr, uint32_t val, void * result);

int arm_reg_writeu16(uint32_t addr, uint16_t val, void * result);

int arm_reg_writeu8(uint32_t addr, uint8_t val, void * result);

