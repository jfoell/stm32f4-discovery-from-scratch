
#pragma once

#include <stdint.h>

int arm_util_memcpy(void * dst, const void * src, uint32_t size, void * result);

int arm_util_memset(void *dst, int byteval, uint32_t size, void * result);

