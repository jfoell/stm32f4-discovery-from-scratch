#pragma once

#define TOOLCHAIN_ISR              __attribute__((interrupt))
#define TOOLCHAIN_NAKED            __attribute__((naked))
#define TOOLCHAIN_NORETURN         __attribute__((noreturn))
#define TOOLCHAIN_SECTION(secname) __attribute__((section(secname)))
