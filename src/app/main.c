#include <stdint.h>

#include "led.h"

static int
app_init(void * result)
{
	if (led_init(0))
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

void
main(void)
{
	if (app_init(0))
	{
		/* do nothing */;
	}
	else
	{
		led_write(0, 1, 0);
		led_write(1, 1, 0);
	}

	while (1);
}

