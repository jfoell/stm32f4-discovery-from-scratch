#include "toolchain.h"

void
TOOLCHAIN_NAKED
TOOLCHAIN_NORETURN
premain(void)
{
	/*
	 * Do early stuff here.
	 *
	 * There is no expectation that any RAM is initialized.  Do everything
	 * yourself and do it carefully.
	 *
	 * This must call crt0_start().
	 */



	/* DO NOT REMOVE */
	extern void crt0_start(void);
	crt0_start();
	while (1);
}

