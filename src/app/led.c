#include <stdint.h>

#include "board.h"
#include "gpio.h"

struct led {
	uint_least8_t portchar;
	uint_least8_t pin;
};

static const struct led led_tbl[] = BOARD_LED_TBL;

#define N_LED (sizeof(led_tbl) / sizeof(struct led))

int
led_init(void * result)
{
	int led = 0;

	while (led < N_LED)
	{
		int portchar = led_tbl[led].portchar;
		int pin = led_tbl[led].pin;

		if (gpio_port_enable(portchar, 0))
		{
			return -1;
		}
		else if (gpio_mode_setout(portchar, pin, 0))
		{
			return -2;
		}

		led++;
	}

	return 0;
}

int
led_write(int led, int value, void * result)
{
	if ((led < 0) || (led >= N_LED))
	{
		return -1;
	}
	else
	{
		int portchar = led_tbl[led].portchar;
		int pin = led_tbl[led].pin;

		return gpio_write(portchar, pin, value, result);
	}
}

