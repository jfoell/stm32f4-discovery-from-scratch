# stm32f4-discovery-from-scratch

From-Scratch bare metal development of a STM32F429ZI on an STM32F4-Discovery board.  Everything is hand written and used to understand all processes of embedded application (especially building, vectors, and runtime and startup).

## Getting started

* need arm-none-eabi-gcc and friends
* need an STM32F4-Discovery development board

## Design Rules
* Everything is hand-written
  * hand-writing is intended to help the contributers to understand what is happening
  * hand-writing exposes many difficult aspects of embedded programming... things that may have been taken for granted
    * Linker Scripts
    * Vector table Design
    * C-runtime startup (crt0)
    * Hardware abstraction
* All functions return `int`
  * unless dictated by Hardware
    * for example, the ISR functions in the vector table return void
  * unless needed for a specific purpose
    * many functions at reset are designed to NOT return a value
* All functions have the ability to return a result via a `void *`, which should be passed as the LAST parameter to the function
  * same exceptions as rule above
* All functions should return a unique negative value upon error
* All Hardware Abstraction files should be unique to a specific target (even if duplicate identical files are needed in separate directories)
* All success states should occur in the `else` clause when using `if..else if..else` statements
* No standard libraries are linked to the application
* No RTOS is used (at this time)
  * An RTOS could be used at a later time, but all c libraries needed will be hand-written
* Follow the coding style that is already set when you start
  * WARNING tabs (width=4) are used for INDENT, spaces are used for ALIGNMENT
