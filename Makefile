
.PHONY all: stm32_f4-discovery

CC_PREFIX:= arm-none-eabi-
CC := ${CC_PREFIX}gcc
CC_ARGS := \
	-g \
	-march=armv7e-m \
	-mthumb \
	-nostartfiles \
	-nostdlib \

CC_INC := \
	-Isrc/toolchain/gcc \
	-Isrc/hal/arm \
	-Isrc/hal/arm/stm32/f429 \
	-Isrc/board/stm32/f4-discovery \
	-Isrc/app \

C_SRC := \
	src/app/led.c \
	src/app/main.c \
	src/app/premain.c \
	src/hal/arm/arm_bba.c \
	src/hal/arm/arm_reg.c \
	src/hal/arm/arm_util.c \
	src/hal/arm/stm32/f429/bus_clock.c \
	src/hal/arm/stm32/f429/crt0.c \
	src/hal/arm/stm32/f429/gpio.c \
	src/hal/arm/stm32/f429/vector.c \

LD_SCRIPT := src/board/stm32/f4-discovery/simple.ld
LD_ARGS := -Wl,-Map=stm32_f4-discovery.map -T ${LD_SCRIPT}

C_OBJ := $(patsubst %.c, build/%.o, ${C_SRC})

build/%.o: %.c
	mkdir -pv $(dir $@)
	${CC} ${CC_ARGS} ${CC_INC} -c -o $@ $<

stm32_f4-discovery.elf: ${C_OBJ}
	${CC} ${CC_ARGS} ${LD_ARGS} -o $@ ${C_OBJ}

stm32_f4-discovery.bin: stm32_f4-discovery.elf
	${CC_PREFIX}objcopy -O binary $< $@

.PHONY stm32_f4-discovery: stm32_f4-discovery.bin

.PHONY clean:
	rm -rf build/

